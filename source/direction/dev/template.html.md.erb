---
layout: markdown_page
title: "Product Section Vision - Dev"
---

- TOC
{:toc}

## Dev Section Overview

The Dev Section is made up of the [Manage](https://about.gitlab.com/direction/manage/index.html), [Plan](https://about.gitlab.com/direction/plan/index.html), and [Create](https://about.gitlab.com/direction/create/index.html) stages of the DevOps lifecycle. These stages mark the left-most side of the DevOps lifecycle and primarily focus on the creation and development of software. The scope for Dev stages is wide and encompasses a number of analyst categories including Value Stream Management, Project Portfolio Management, Enterprise Agile Planning Tools, Source Code Management, IDEs, Design Management, and even ITSM. It is difficult to truly estimate TAM for the Dev Section as our scope includes so many components from various industries, but most research indicates the estimated [TAM](https://docs.google.com/spreadsheets/d/1HYi_l8v-wTE5-BUq_U29mm5aWNxnqjv5vltXdT4XllU/edit?usp=sharing) in 2019 is roughly ~$3B, growing to ~$7.5B in 2023 (26.5% CAGR.)

Based on [DevOps tool revenue](https://drive.google.com/file/d/1VvnJ5Q5PJzPKZ_oYBHGNuc6D7mtMmIZ_/view) at the end of 2018 and comparing to GitLab ARR at the end of FY20 Q3, our estimated market share is approximately ~1.5% based on revenue. (Note: this assumes we can attribute 100% of revenue to Dev stages) Market share based on source code management alone would most likely range far higher. [BOZO - find data]

Nearly [half of organizations](https://drive.google.com/file/d/17ZSI2hGg3RK168KHktFOiyyzRA93uMbd/view?usp=sharing) have still not adopted DevOps methodologies, despite [data](https://drive.google.com/file/d/17MNecg84AepxWlSDB5HjNBrCJggaS9tP/view?usp=sharing) that indicates far higher revenue growth for organizations that do so. Migrating a code base to a modern Git backed source control platform like GitLab can often be the first step in a DevOps transformation. As such, we must provide industry leading solutions in source code and code review as this is not only the entry into DevOps for our customers, but typically the entry into the GitLab platform, for customers who are starting DevOps journeys, customers who are running mature DevOps shops, and customers in between. Once a user has begun utilizing repositories and code review features like Merge Requests, they often move “left” and “right” to explore and utilize other capabilities in GitLab such as CI and project management features.

Per our [Stage Monthly Active User Data](https://app.periscopedata.com/app/gitlab/425984/Month-over-Month-Overestimated-SMAU), Manage and Create have the highest usage amongst GitLab stages. As such, these stages must focus on security fixes, bug fixes, performance improvements at disproportionate rate compared to other areas of product investment at Gitlab. [Enter info on bug/security issues.] Plan, while introduced in 2011 with the release of issues, still falls far behind market leaders who have better experiences for sprint management, portfolio management, roadmapping, and enforced workflows.

Other areas, such as Value Stream Management are nascent to both GitLab and the market, and will require more time devoted to executing problem and solution validation tasks. Moving into 2020, each Stage will require significant investment to keep up with security issues to ensure a performant experience while also shipping validated direction items at a high velocity to maintain market relevance. 


## Resourcing and Investment
The current investment into Dev is:
* 75 team members in [Development](https://about.gitlab.com/company/team/?department=dev-section)
* 10 team members in [UX](https://about.gitlab.com/company/team/?department=dev-ux-team)
* 10 team members in [Product Management](https://about.gitlab.com/company/team/?department=dev-pm-team)
* 6 team members in [QE](https://about.gitlab.com/company/team/?department=dev-qe-team)

These numbers are in accordance with our [2019 product development plan](https://docs.google.com/spreadsheets/d/1MUR2IhPxS0tQCKYMlJSpC0uA0spEYBPA7V--CzbWy8M/edit#gid=1845624614).

As points of reference, GitHub has [over 800 employees](https://github.com/about/facts) and primarily competes against us in only the Create and Plan stages. Atlassian has [3,061 employees](https://s2.q4cdn.com/141359120/files/doc_financials/2019/q2/TEAM-Q2-2019-shareholder-letter.pdf) and competes with us primarily in Plan and Create stages. 

## Dev Section SWOT Analysis & Challenges


### Strengths
* End to end platform with seamless connections to downstream sections like CI/CD, Ops, Secure, and Defend.
* Strong analyst relationships allowing us to help define nascent markets like Value Stream Management.
* Exceptionally talented product team with deep industry expertise. 
* [Open core](https://about.gitlab.com/2016/07/20/gitlab-is-open-core-github-is-closed-source/), with both self-managed and SaaS options.
* Expansive feature set and roadmap that aligns to validated market needs. 
* We’ve chosen to invest in a storage architecture ([Gitaly](https://about.gitlab.com/2018/09/12/the-road-to-gitaly-1-0/)) that will lead to future performance enhancements.

### Weaknesses

* Our SaaS product (GitLab.com) is not yet enterprise grade and several improvements are needed for self-managed deployments for more widespread enterprise adoption
* Performance issues continue to mount in various areas (ex: MR load times, diff rendering) as we add new features and code
* Some areas of Dev are not competitive, for example Wikis, Snippets, and Roadmaps.
* Our company name may be limiting our opportunities for the Plan stage by creating the sense that the features only work for the software development lifecycle. We may need to consider improving how we package capabilities for customer segments who desire to use GitLab for project or portfolio management.

### Opportunities

* Consideration of new business models/licensing plans could lead to additional adoption of features.
* Take a leadership position in Value Stream Management, a nascent market with many vendors who aren’t doing it well.
* Big opportunity to grow and/or shift usage to SaaS, allowing customers to receive value more quickly from GitLab without provisioning and maintaining their own instance.
* Disrupting dominant legacy designs by creating a modern, loveable system for requirements management, workflows, and permissions.
* Creating an “easy migration” path from other popular DevOps tools like Jenkins, JIRA, GitHub would expedite our market share growth.
* Increasing investment into Wikis, Gitaly, Compliance, and Boards will provide more focus to these areas, resulting in additional IACV.

### Threats

* Onboarding new team members may lead to a slow down in velocity if not managed carefully
* Other companies such as GitHub and Atlassian begin to innovate more quickly than they have in previous years; closing the cracked window of opportunity for us to gain market share.
* If we stop talking to customers to better understand their problems and pain points, we will continue to ship code and features at a high velocity but the things we ship may not be adding value to our customers. This is known as the [build trap](https://melissaperri.com/blog/2014/08/05/the-build-trap).
* Dev tools start to become commoditized with IaaS providers giving them away for free with IaaS/cloud services contracts.
* The Dev section currently has the highest volume of high priority defects and security issues; which will naturally impact the ability to ship direction items as quickly as expected. This could result in to slower business growth as direction items are typically tied to IACV or customer upsell/retention opportunities.

## Vision Themes
Our vision for the Dev section is to provide the world’s best product creation platform. We believe we have a massive opportunity to change how cross-functional, multi-level teams collaborate by providng an experience to break down organizational silos and enable better collaboration. We want to deliver a solution that enables higher quality products to be created faster.

### Efficient and Automated Code Review
Code review should be a delightful experience for all involved in the process. Over time, we expect the code review process to evolve from where it is today, to a mostly automated process in the future. Along the way, incremental improvements will occur, where developer platforms like GitLab will focus on performance and usability of the code review tools. Code review should be an efficient process, and the easier GitLab can make code review, the more efficient dev teams become. Although unproven, better code review should reduce the number of bugs and increase the amount of higher quality features an organization can ship. The code review process will continue to provide a venue for developers to learn and collaborate together.

As examples, GitLab will:
* Load large, multi-file diffs faster than any other comparable product on the market
* Provide tailored insights to the code reviewer, alerting them to the most important areas to review
* Allow for client and server side evaluation of the code where possible and integrate it into the code review process

### Measurement and Increased Efficiency of the SDLC
Peter Drucker has stated “If you can’t measure it, you can’t improve it.” Many software development teams have no way of measuring their efficiency, and even if they do, there is not enough feedback, information, or actionable insights to improve the efficiency of their team. Even then, once efficiency is improved, it can be difficult to tell if a team’s performance is good or bad, as there is often no point of comparison. Even the best performing team in an organization could be worse than the competition. Increasing efficiency is paramount to companies increasing their **time to value** and helping organizations answer **“Is my DevOps transformation working?”**

We believe efficiency can be improved in two ways. The first way is improving the existing value stream activities and making them more efficient. This focuses on making existing activities as fast as possible. The second way is to question and change the value stream into higher value added activities at each step. GitLab’s vision is to help answer both of these questions; “Am I doing things fast enough?” and “Am I doing the right things?”

As examples, GitLab will:
* Provide easy to use and customizable tools to measure the efficiency of the DevOps lifecycle
* Provide insight into areas of waste where teams can improve
* Provide recommendations based on large data sets of other teams using GitLab for comparison

### DevOps for More Personas
DevOps started with the merging of Development and Operations and has since been augmented to include Security in some circles, highlighting DevSecOps as the next trend. There are many other personas that are involved in software development such as product managers, project managers, product designers, finance, marketing, procurement, etc. These personas will continue to expand until nearly every role at knowledge work companies touch some facet of the DevOps lifecycle. Over time the industry will shift as it realizes teams who work out of the same platform/set of tools are more efficient and deliver faster business and customer value.

Each persona of the DevOps lifecycle should ultimately be treated as a first class citizen in GitLab. 

As examples, GitLab will:
* Provide a better experience for project management workflows
* Provide a space for product designers to design and collaborate on those designs
* Consider non-traditional industries who are also undergoing DevOps transformations such as Healthcare, Manufacturing, Aerospace, etc.
* Provide a Web IDE experience that is able to run the GDK, serving collaborators of all skill sets and hardware, allowing them to contribute to GitLab

### Ease of Use _and_ Flexibility
While we will continue to solve for the modern DevOps knowledge worker first[BOZO - LINK], most enterprise customers have customized requirements that GitLab currently doesn’t solve for today. This is a wide ranging set of customized controls that spans systems such as permissions, approvals, compliance, governance, workflows, and requirements mapping. It is our belief these needs will exist for many years to come and we will need to incorporate these to truly become a flexible DevOps platform that caters to enterprise requirements. We will strive to do this in ways that are modern and where possible adhere to a [“convention over configuration”](https://about.gitlab.com/handbook/product/#convention-over-configuration) approach, living with the cognitive dissonance that sometimes flexibility will be required in areas we have not been willing to venture into thus far.

Additionally, compliance, auditing, and surfacing evidence of security/compliance posture will become more important as more GDPR-like legislation is enacted and passed into law. GitLab should make it easy to not only surface and deliver evidence for GitLab controls (i.e. who has access to GitLab, who did what on what group, etc.) but also to track and manage compliance requirements for various legislation our customers may be bound to.

As examples, GitLab will:
* Provide customizable workflows, unlocking enforcement and insight into these workflows
* Provide for more customizable and fine grained permissions
* Provide logs for everything that’s done within GitLab and allow those to be accessible via the API and UI

## 3 Year Strategy
In three years the Dev Section market will:
* Centralize around git as the version control of choice for not only code, but for design assets, gaming, silicon designs, and AI/ML models.
* Have a market leader emerge in the value stream management space. Currently the market is fragmented with most players focused on integrations into various DevOps tools.
* Adopt a mindset shift from project management to product management

As a result, in three years, GitLab will:
* Provide a next-generation, highly performant git backed version control system for large assets, such as ML models. Our goal in three years should be to host the most repositories of these non-code assets.
* Emerge as the leader in VSM and be recognized in the industry by customers and analysts as such. Our goal in three years should be to provide the best insights into the product development process that no other tool can come close to as we have a [unified data model](https://www.ca.com/en/blog-itom/what-is-a-unified-data-model-and-why-would-you-use-it.html) due to GitLab being a single platform.

## 1 Year Plan (What’s Next for Dev)
In the next 12 months, the Dev Section must focus on:

### Manage

**Enterprise readiness:** GitLab must be seen as a platform that the enterprise can use out of the box for both GitLab.com and self-managed options. We must focus on delivering SSO integration and enforcement, anti-abuse limits for GitLab.com, creating a compliance dashboard, and deliver additional permissions options for enterprise users.

**Lowering time to production for our customers:** Improvements of productivity and code analytics over the next 12 months will allow our customers to drill down and identify sources of waste in their existing process. Within 12 months, GitLab customers will be able to firmly answer how much their time to production metrics have improved.

### Plan

**Displacing JIRA:** Quite simply, FY21 is the year where JIRA can be fully replaced by GitLab Plan features. In the next 12 months, we will deliver enforced workflows, a better roadmap experience, and improvements to boards in order to begin the [flywheel](https://www.jimcollins.com/concepts/the-flywheel.html) of obtaining non-trivial amounts of marketshare from Atlassian.

**Enhance Portfolio and Project Roadmaps**: Provide easy to use, cross-team roadmaps at the portfolio, project, and epic level; allowing users across the org to see how work is progressing and identify dependencies and blockers. Organize and prioritize work though dynamic roadmaps in real time. 

**Easy Top Down Planning**: Enhanced portfolio management experience allowing you to start planning from the top, creating initiatives, projects, and epics and laying them out on a roadmap prior to issues and milestones being created. Provide analytics at each level, and allow linking of each to provide deeper dependency mapping across multiple teams and projects. Enable users to create strategic initiatives and assign work, impact, and resources to each in order to make the right business decisions.

**Reporting and Analytics**: Provide dashboarding and analytics for project and portfolio management, allowing business to track and communicate progress on work in flight, capacity of teams and projects, and overall efficiency across their full portfolio. 

### Create

**Enhancing the code review experience:** In the next 12 months, we must focus code review to be more performant and more intelligent. We will do this by investing in [performance improvements](@jramsay Link), adding additional code review functionality such as jump to definition, semantics analysis, identifying references, displaying function documentation and type signatures, and adding support for first class reviewers.

**Making large files “just work” in Git:** In order to gather more market share from industries who currently use Perforce or SVN, we must invest into making the large file experience in Git excellent. It should “just work” without configuration or specialized hardware.

**Invest into our Wiki product:** Many customers currently utilize Confluence for knowledge bases, team wikis, and company handbooks. Our first step in making the GitLab Wiki more competitive is making them available at the group level. We will focus on this in tandem with the JIRA displacement activities in Plan. If customers feel GitLab can be a replacement for JIRA, but not Confluence, we will fail in abolishing the Atlassian relationship.

**Enable easier contribution to GitLab:** Contributing to GitLab requires users to setup and run the [GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit) locally. This is cumbersome and typically requires multiple hours of debugging with more senior engineers. While the process of streamlining the GDK locally should be advanced, GitLab should also provide the GDK as part of the Web IDE experience. Allowing contributors to quickly spin up feature branches should encourage more contributions from non-engineering GitLab team members as well as the wider community.

Choosing to invest in these areas in 2020 means we will not choose to:
* Invest into features that help companies answer “am I doing the right activities?” Answering this question is something we will focus on in years two and three of the VSM plan.
* Treat ML models as first class citizens in GitLab. Instead, we will focus on getting large assets to become performant via improvements to Gitaly. Once this is completed, we will focus on ML models.
* Provide recommendations where customers can improve their efficiency in the DevOps lifecycle. This will likely require comparisons amongst many GitLab users and an AI engine to make intelligent recommendations. These improvements will come in years two and three of the VSM plan.

<%= direction %>